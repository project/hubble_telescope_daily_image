CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Innitial Configuration

INTRODUCTION
------------

This is a very simple module that creates a block hat displays a Hubble 
Space Telescope image each day of the year. The image changes every day.


REQUIREMENTS
------------

This module depends on the drupal core block module.


INSTALLATION
------------

This module is installed like any drupal module and has no specific
installation instructions.

INNITIAL CONFIGURATION
----------------------
The block's configuration page lets you set the dimensions of the picture. 
It also lets you choose if you want to link the picture to the Hubble official 
website and to display the picture's title.
